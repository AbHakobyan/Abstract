﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.Lesson.Figure.Shapes
{
    abstract class Shape
    {
        protected Shape()
        {
            width = 5;
            height = 5;
        }

        protected int width;
        public virtual int Width
        {
            get => width;
            set
            {
                if (value < 0)
                    width = 5;
                else
                    width = value;
            }
        }

        protected int height;
        public virtual int Height
        {
            get => height;
            set
            {
                if (value < 0)
                    height = 5;
                else
                    height = value;
            }
        }

        //public virtual void Draw() { }
        public abstract void Draw();
    }
}
