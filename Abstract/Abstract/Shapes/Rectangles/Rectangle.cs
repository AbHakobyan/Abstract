﻿using System;

namespace Mic.Lesson.Figure.Shapes.Rectangles
{
    class Rectangle : Shape
    {
        public override void Draw()
        {
            for (int i = 0; i < Height; i++)
            {
                Console.WriteLine(new string('*', Width));
            }
        }
    }
}
