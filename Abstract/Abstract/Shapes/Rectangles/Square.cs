﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.Lesson.Figure.Shapes.Rectangles
{
    class Square : Rectangle
    {
        public override int Height
        {
            get => base.Height;
            set
            {
                height = value;
                width = value;
            }
        }

        public override int Width
        {
            get => width;
            set
            {
                height = value;
                width = value;
            }
        }
    }
}
