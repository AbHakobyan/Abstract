﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.Lesson.Figure.Shapes.Table
{
    class Table : Shape
    {
        public override void Draw()
        {
            for (int i = 0; i < Height; i++)
            {
                Console.Write("-");
                for (int j = 0; j < Width; j++)
                {
                    Console.Write("|");
                }
                Console.WriteLine();
            }
        }
    }
}
