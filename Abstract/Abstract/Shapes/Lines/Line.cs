﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.Lesson.Figure.Shapes.Lines
{
    class Line : Shape
    {
        public sealed override int Width
        {
            get => width;
            set
            {
                width = value;
                height = 0;
            }
        }

        public sealed override int Height
        {
            get => height;
            set
            {
                height = value;
                width = 0;
            }
        }

        public bool IsHorizontal => Width > 0;
        //{
        //    get
        //    {
        //        if (Width > 0)
        //        {
        //            return true;
        //        }
        //        else
        //        {
        //            return false;
        //        }
        //    }
        //}

        public override void Draw()
        {
            if (IsHorizontal)
            {
                for (int i = 0; i < Width; i++)
                {
                    Console.Write("-");
                }
            }
            else
            {
                for (int i = 0; i < Height; i++)
                {
                    Console.WriteLine("|");
                }
            }
        }
    }
}
