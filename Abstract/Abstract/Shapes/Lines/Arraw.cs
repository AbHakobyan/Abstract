﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.Lesson.Figure.Shapes.Lines
{
    class Arraw : Line
    {
        public override void Draw()
        {
            base.Draw();

            if (IsHorizontal)
                Console.Write(">");
            else
                Console.WriteLine("V");
        }
    }
}
