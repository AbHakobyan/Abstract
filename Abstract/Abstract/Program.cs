﻿using Mic.Lesson.Figure.Shapes;
using Mic.Lesson.Figure.Shapes.Lines;
using Mic.Lesson.Figure.Shapes.Rectangles;
using Mic.Lesson.Figure.Shapes.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.Lesson.Figure
{
    class Program
    {
        static void Main(string[] args)
        {
            Table table = new Table
            {
                Height = 10,
                Width = 10
            };
            table.Draw();

            //Dooble_Arrow dArrow = new Dooble_Arrow();
            //dArrow.Height = 15;
            //dArrow.Width = 10;
            //dArrow.Draw();

            Shape shape = new Square();
            shape.Height = 10;
            shape.Width = 3;

            shape.Draw();

            Console.ReadLine();
        }
    }
}
